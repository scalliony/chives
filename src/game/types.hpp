#pragma once

#include "utils/db.hpp"
#include <spdlog/fmt/fmt.h>
#include <variant>
#include <unordered_map>

namespace game {

using coord_t = db::integer;

struct cell {
    coord_t x, y;

    inline cell operator+(const cell& c) const { return cell{x + c.x, y + c.y}; }
    inline cell operator-(const cell& c) const { return cell{x - c.x, y - c.y}; }
    inline bool operator!=(const cell& c) const { return x != c.x || y != c.y; }
    inline bool operator==(const cell& c) const { return !operator!=(c); }
};
struct chunk_pos {
    coord_t x, y;

    static inline chunk_pos From(const cell &c, uint16_t cs) { return chunk_pos{ c.x >> cs, c.y >> cs }; }
    static inline coord_t Size(uint16_t cs) { return 1u << cs; }

    inline chunk_pos operator+(const chunk_pos& c) const { return chunk_pos{x + c.x, y + c.y}; }
    inline chunk_pos operator-(const chunk_pos& c) const { return chunk_pos{x - c.x, y - c.y}; }
    inline bool operator!=(const chunk_pos& c) const { return x != c.x || y != c.y; }
    inline bool operator==(const chunk_pos& c) const { return !operator!=(c); }
};

struct area {
    cell min, max;

    static inline area From(const chunk_pos &ck, uint16_t cs) { return area{
        cell{ ck.x << cs, ck.y << cs },
        cell{ ((ck.x + 1) << cs) - 1, ((ck.y + 1) << cs) - 1 }
    }; }
};

template<typename E>
E from_string(const std::string& s);

enum class Rotation: unsigned char { Up, Right, Down, Left, Count };
inline Rotation Rotated(Rotation r, int n) {
    constexpr auto C = (int)Rotation::Count;
    return (Rotation)(((((int)r + n) % C) + C) % C);
}
inline cell Direction(Rotation r) {
    switch (r) {
        case Rotation::Up:
            return cell{0, 1};
        case Rotation::Right:
            return cell{1, 0};
        case Rotation::Down:
            return cell{0, -1};
        case Rotation::Left:
            return cell{-1, 0};
        default:
            return cell{0, 0};
    }
}
inline const char* to_string(Rotation t) {
    switch (t) {
    case Rotation::Up:
        return "up";
    case Rotation::Right:
        return "right";
    case Rotation::Down:
        return "down";
    case Rotation::Left:
        return "left";
    default:
        return "bad enum";
    }
}
template<>
inline Rotation from_string<Rotation>(const std::string& s) {
    static std::unordered_map<std::string, Rotation> const table = {
        {"up", Rotation::Up}, {"right", Rotation::Right},
        {"down", Rotation::Down}, {"left", Rotation::Left}
    };
    auto it = table.find(s);
    if (it != table.end()) {
        return it->second;
    } else
        return static_cast<Rotation>(-1);
}

enum class EventType { Tick, Move, Rotate, Log };
inline const char* to_string(EventType t) {
    switch (t) {
    case EventType::Tick:
        return "tick";
    case EventType::Move:
        return "move";
    case EventType::Rotate:
        return "rotate";
    case EventType::Log:
        return "log";
    default:
        return "bad enum";
    }
}
[[nodiscard]]
inline auto to_taopq_param(const EventType& t) noexcept {
    return to_string(t);
}

enum class EntityType { Rock, Bot, Building };

inline const char* to_string(const EntityType& t) {
    switch (t) {
    case EntityType::Rock:
        return "rock";
    case EntityType::Bot:
        return "bot";
    case EntityType::Building:
        return "building";
    default:
        return "bad enum";
    }
}
[[nodiscard]]
inline auto to_taopq_param(const EntityType& t) noexcept {
    return to_string(t);
}
template<>
inline EntityType from_string<EntityType>(const std::string& s) {
    static std::unordered_map<std::string, EntityType> const table = {
        {"rock", EntityType::Rock}, {"bot", EntityType::Bot},
        {"building", EntityType::Building}
    };
    auto it = table.find(s);
    if (it != table.end()) {
        return it->second;
    } else
        return static_cast<EntityType>(-1);
}

struct entity {
    db::bit_int id;
    EntityType type;
};

using chunk_cells = std::unordered_map<cell, entity>;
constexpr auto surrounding_radius = 1;
constexpr auto surrounding_size = 1+2*surrounding_radius;
using chunk_surrounding = std::array<std::array<const chunk_cells *, surrounding_size>, surrounding_size>;
inline bool InSurrounding(int x, int y) {
    return x >= -surrounding_size && x <= surrounding_size &&
           y >= -surrounding_size && y <= surrounding_size;
}
inline const chunk_cells*& At(chunk_surrounding& cs, int x, int y) {
    assert(InSurrounding(x, y));
    return cs[x+surrounding_radius][y+surrounding_radius];
}

namespace action
{
    struct rotate {
        bool left;
    };
    struct move {
        coord_t dist;
    };
    struct str_ref {
        /// Index of Bot::context_t::strings
        uint idx;
    };
    struct log: str_ref { };
    struct crash: str_ref { };

    using any = std::variant<rotate, move, log, crash>;
    constexpr auto any_size = sizeof(any);
    static_assert(any_size <= 8, "some big action (maybe alloc)");
}
}

namespace std {

template<>
struct hash<game::cell> {
    std::size_t operator()(const game::cell& a) const noexcept {
        std::size_t h1 = std::hash<game::coord_t>{}(a.x);
        std::size_t h2 = std::hash<game::coord_t>{}(a.y);
        return h1 ^ (h2 << 1);
    }
};

template<>
struct hash<game::chunk_pos> {
    std::size_t operator()(const game::chunk_pos& a) const noexcept {
        std::size_t h1 = std::hash<game::coord_t>{}(a.x);
        std::size_t h2 = std::hash<game::coord_t>{}(a.y);
        return h1 ^ (h2 << 1);
    }
};

}

namespace fmt {

template <>
struct formatter<game::chunk_pos> {
    constexpr auto parse(format_parse_context& ctx) {
            return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const game::chunk_pos& p, FormatContext& ctx) {
            return format_to(ctx.out(), "({}, {})", p.x, p.y);
    }
};

}

namespace tao::pq {

template<>
struct result_traits<game::EntityType> {
    [[nodiscard]] static game::EntityType from(const char* value) {
        return game::from_string<game::EntityType>(value);
    }
};

template<>
struct result_traits<game::Rotation> {
    [[nodiscard]] static game::Rotation from(const char* value) {
        return (game::Rotation)result_traits<unsigned int>::from(value);
    }
};

}