#pragma once

#include "utils/db.hpp"
#include <tao/json.hpp>
#include <tao/pq/table_writer.hpp>

namespace tao::json {
   std::string to_taopq_param(const value& v) {
      return to_string(v);
   }
}

namespace tao::pq {

template<>
struct result_traits<json::value> {
    [[nodiscard]] static json::value from( const char* value ) {
        return json::from_string( value );
    }
};

class bytea {
private:
    std::size_t m_size;
    unsigned char* m_data;

public:
    explicit bytea(const char* value):
        m_size(0), m_data(PQunescapeBytea((unsigned char*)value, &m_size)) {
        if( m_data == nullptr )
            throw std::bad_alloc();
    }
    ~bytea() {
        PQfreemem(m_data);
    }

    bytea(const bytea&) = delete;
    auto operator=(const bytea&) -> bytea& = delete;

    [[nodiscard]] auto size() const noexcept {
        return m_size;
    }

    [[nodiscard]] auto data() const noexcept -> const unsigned char* {
        return m_data;
    }

    [[nodiscard]] auto operator[]( const std::size_t idx ) const noexcept -> unsigned char {
        return m_data[idx];
    }
};

template<>
struct result_traits<bytea> {
    [[nodiscard]] static auto from(const char* value) {
        return bytea( value );
    }
};

}

namespace db {

namespace smt {
    constexpr auto EVENT = "event";
    constexpr auto BOTS = "bots";
    constexpr auto CHUNK = "chunk";
    constexpr auto CELLS = "cells";
    constexpr auto ROTATE_BOT = "rotate_bot";
    constexpr auto BOT_POS = "bot_pos";
    constexpr auto MOVE_POINT = "move_point";
    constexpr auto ADD_CHUNK = "add_chunk";
    constexpr auto FINISH_CHUNK = "finish_chunk";
    constexpr auto PROGRAM = "program";
    constexpr auto SET_PROG_RES = "set_prog_res";
    constexpr auto INC_SNAPSHOT = "inc_snapshot";
    constexpr auto SNAPSHOT_STATUS = "snapshot_status";
}

void Prepare(const connection& c) {
    c->prepare(smt::EVENT, "INSERT INTO event(type, data) VALUES ($1, $2)");
    c->prepare(smt::BOTS, "SELECT bot.id, bot.program_id, MIN(cell.x), MIN(cell.y), bot.rotation "
        "FROM bot JOIN cell ON entity_id = bot.id GROUP BY bot.id, bot.program_id, bot.rotation");
    c->prepare(smt::CHUNK, "SELECT generated FROM chunk WHERE x = $1 AND y = $2");
    c->prepare(smt::CELLS, "SELECT x, y, id, type FROM cell JOIN entity "
        "ON id = entity_id WHERE x >= $1 AND x <= $2 AND y >= $3 AND y <= $4");
    c->prepare(smt::ROTATE_BOT, "UPDATE bot SET rotation = (((rotation + $1) % 4) + 4) % 4 "
        "WHERE id = $2 RETURNING rotation");
    c->prepare(smt::BOT_POS, "SELECT MIN(cell.x), MIN(cell.y), bot.rotation FROM bot "
        " JOIN cell ON entity_id = bot.id WHERE bot.id = $1 GROUP BY bot.id, bot.rotation");
    c->prepare(smt::MOVE_POINT, "UPDATE cell SET x = $1, y = $2 WHERE entity_id = $3");
    c->prepare(smt::ADD_CHUNK, "INSERT INTO chunk(x, y, generated) VALUES ($1, $2, FALSE)");
    c->prepare(smt::FINISH_CHUNK, "UPDATE chunk SET generated = TRUE "
        "WHERE x = $1 AND y = $2 AND generated = FALSE");
    c->prepare(smt::PROGRAM, "SELECT CASE WHEN (checked = '') IS NOT FALSE THEN data "
        "ELSE '' END as code, checked FROM program WHERE id = $1");
    c->prepare(smt::SET_PROG_RES, "UPDATE program SET checked = $1 WHERE id = $2");
    c->prepare(smt::INC_SNAPSHOT, "UPDATE config set value=CAST((CAST(value AS integer)+$1) AS text) "
        "WHERE key = 'snapshot_inc'");
    c->prepare(smt::SNAPSHOT_STATUS, "SELECT (SELECT CAST(value AS integer) FROM config "
        "WHERE key = 'snapshot_inc') + (SELECT COUNT(*) FROM event WHERE id > "
        "(SELECT COALESCE(MAX(event_id), 0) FROM snapshot.index)) > $1");
}

inline void _assert([[maybe_unused]] const result& r, [[maybe_unused]] size_t n = 1) {
    assert(r.has_rows_affected() && r.rows_affected() == n);
}

template<typename T>
inline std::unordered_map<std::string, std::string> GetConfig(const T& t) {
    const result res = t->execute("SELECT key, value FROM config");
    return res.unordered_map<std::string, std::string>();
}
template<typename T>
inline int RegisterWorker(const T& t) {
    const result res = t->execute("UPDATE config SET value=CAST((CAST(value AS integer)+1) AS text) "
        "WHERE key = 'worker_count' RETURNING CAST(value AS integer)");
    return res.as<int>();
}
template<typename T>
inline void UnregisterWorker(const T& t) {
    t->execute("UPDATE config SET value=CAST(GREATEST(0, CAST(value AS integer)-1) AS text) "
        "WHERE key = 'worker_count'");
}

template<typename T>
inline void EmitEvent(const T& t, game::EventType e, const tao::json::value& v) {
    _assert(t->execute(smt::EVENT, game::to_string(e), v));
}

template<typename T, typename F>
inline void ForBots(const T& t, F f) {
    for(const row& row: t->execute(smt::BOTS))
        f(row.get<bit_int>(0), row.get<integer>(1), game::cell{
            row.get<integer>(2), row.get<integer>(3) }, (game::Rotation)row.get<unsigned char>(4));
}

template<typename T>
inline game::Rotation RotateBot(const T& t, bit_int id, bool left) {
    const result res = t->execute(smt::ROTATE_BOT, left ? -1 : 1, id);
    return (game::Rotation)res.as<unsigned char>();
}

template<typename T>
inline std::pair<game::cell, game::Rotation> GetBotPos(const T& t, bit_int id) {
    const result res = t->execute(smt::BOT_POS, id);
    const row row = res.at(0);
    return {game::cell{row.get<integer>(0), row.get<integer>(1)}, row.get<game::Rotation>(2)};
}

template<typename T>
inline std::optional<bool> FindChunk(const T& t, game::chunk_pos c) {
    const result res = t->execute(smt::CHUNK, c.x, c.y);
    return res.optional<bool>();
}

template<typename T>
inline void RetryChunk(const T& t, game::chunk_pos c) {
    t->execute("DELETE FROM chunk WHERE x = $1 AND y = $2 AND generated = FALSE", c.x, c.y);
}

template<typename T>
inline bool AddChunk(const T& t, game::chunk_pos c) {
    const auto res = t->execute(smt::ADD_CHUNK, c.x, c.y);
    return res.has_rows_affected() && res.rows_affected() > 0;
}

template<typename T>
inline void FinishChunk(const T& t, game::chunk_pos c) {
    _assert(t->execute(smt::FINISH_CHUNK, c.x, c.y));
}

template<typename T>
inline std::unordered_map<game::cell, game::entity> GetCells(const T& t, game::area a) {
    const result rows = t->execute(smt::CELLS, a.min.x, a.max.x, a.min.y, a.max.y);
    std::unordered_map<game::cell, game::entity> cells;
    cells.reserve(rows.size());
    for(const row& row: rows) {
        cells.emplace(
            game::cell{row.get<integer>(0), row.get<integer>(1)},
            game::entity{row.get<bit_int>(2), row.get<game::EntityType>(3) });
    }
    return cells;
}

template<typename T>
inline void MovePointEntity(const T& t, bit_int id, game::cell p) {
    _assert(t->execute(smt::MOVE_POINT, p.x, p.y, id));
}

using write_cell_fn = std::function<void(const game::cell&)>;
template<typename T, typename F>
inline size_t WriteCells(const T& t, F f) {
    t->execute("CREATE TEMPORARY TABLE temp_cell(x integer NOT NULL, y integer NOT NULL, tmp_id serial)");
    tao::pq::table_writer tw(t, "COPY temp_cell(x, y) FROM STDIN");
    f([&](const game::cell &p) { tw.insert(fmt::format("{}\t{}\n", p.x, p.y)); });
    const auto n_rows = tw.finish();
    t->execute("CREATE TEMPORARY TABLE temp_entt(tmp_id serial, entity_id bigint)");
    t->execute("WITH entt AS (INSERT INTO entity(type) SELECT 'rock' FROM temp_cell RETURNING id) "
        "INSERT INTO temp_entt(entity_id) SELECT id FROM entt");
    //MAYBE: Find a way to directly into temp_cell.entity_id or even insert cells
    t->execute("INSERT INTO cell(x, y, entity_id) SELECT x, y, entity_id FROM temp_cell "
        "JOIN temp_entt ON temp_entt.tmp_id = temp_cell.tmp_id ON CONFLICT DO NOTHING");
    t->execute("DROP TABLE temp_cell");
    t->execute("DROP TABLE temp_entt");
    return n_rows;
}

template<typename T>
inline result Program(const T& t, integer id) {
    return t->execute(smt::PROGRAM, id);
}

template<typename T>
inline void SetProgramResult(const T& t, integer id, const std::string& v) {
    _assert(t->execute(smt::SET_PROG_RES, v, id));
}

template<typename T>
inline void RequestSnapshot(const T& t, size_t power) {
    _assert(t->execute(smt::INC_SNAPSHOT, power));
}

template<typename T>
inline bool MustSnapshot(const T& t, size_t limit) {
    const result res = t->execute(smt::SNAPSHOT_STATUS, limit);
    return res.as<bool>();
}

template<typename T>
inline void DoSnapshot(const T& t) {
    t->execute("CALL do_snapshot()");
    t->execute("UPDATE config set value='0' WHERE key = 'snapshot_inc'");
}

}