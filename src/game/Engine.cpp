#include "Engine.hpp"
#include "API.hxx"
#include "SQL.hxx"
#include <signal.h>
#include <chrono>
#include <thread>
#include <FastNoiseLite.h>
#include "version.h"

using namespace game;

static const char* _concurrency_names[] = {"procedural", "thread", "process", "full", NULL};
const char **Engine::concurrency_names = _concurrency_names;

Engine::Engine(db::connection&& db): database(std::move(db)) {
    assert(database->is_open());

    const auto cfs = db::GetConfig(database);
    const auto get = [&](const std::string& key) -> std::string {
        if (const auto it = cfs.find(key); it != cfs.end())
            return it->second;

        throw std::runtime_error(fmt::format("missing config key '{}'", key));
    };

    if (auto version = get("version"); version > CHIVES_VERSION)
        throw std::runtime_error(fmt::format("not up to date requires {} {}", version));

    config.seed = std::stoi(get("seed"));
    config.chunk_scale = std::stoul(get("chunk_scale"));
    config.tick_per_second = std::stof(get("tick_per_second"));
    config.snapshot_limit = std::stoul(get("snapshot_limit"));
    get("snapshot_inc");
    {
        config.concurrency = (config_t::concurrency_t)CHAR_MAX;
        const auto level = get("concurrency");
        for (size_t i = 0; i <= config_t::Full; i++) {
            if (strcmp(level.c_str(), concurrency_names[i]) == 0) {
                config.concurrency = (config_t::concurrency_t)i;
                break;
            }
        }
        if ((int)config.concurrency > config_t::Full)
            throw std::runtime_error(fmt::format("unexpected concurrency level {}", level));
    }
    get("worker_count");
    if (db::RegisterWorker(database) > 1 && (config.concurrency & config_t::Process) == 0) {
        db::UnregisterWorker(database);
        throw std::runtime_error("cannot run multiple workers without process level concurrency");
    }

    db::Prepare(database);
}
Engine::~Engine() {
    db::UnregisterWorker(database);
}

constexpr auto init_ops = 1000;
constexpr auto tick_ops = 100;

static bool running = true;
void handle_signal(int) { running = false; }

void Engine::run() {
    assert(database->is_open());

    signal(SIGINT, handle_signal);
    signal(SIGTERM, handle_signal);

    spdlog::info("Worker ready");
    while (running) {
        const auto startTime = std::chrono::steady_clock::now();
        spdlog::trace("Tick");
        tick();
        spdlog::trace("Done in {}ms", std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - startTime).count());
        const auto tickDuration = std::chrono::milliseconds((int)(1000 / config.tick_per_second));
        const auto sleepDuration = tickDuration / 10;
        while (running && std::chrono::steady_clock::now() - startTime < tickDuration) {
            std::this_thread::sleep_for(sleepDuration);
        }
    }
    spdlog::warn("Worker stopped");
}

void Engine::tick() {
    const auto procedural = config.concurrency == config_t::Procedural;
    if (!procedural) {
        cache.chunks.clear();
        //TODO: ADVISORY-LOCKS
    }
    if (db::MustSnapshot(database, config.snapshot_limit)) {
        spdlog::info("Creating snapshot");
        //MAYBE: spawn a new client to do than
        db::DoSnapshot(database);
    }
    db::EmitEvent(database, EventType::Tick, tao::json::null);
    //TODO: unlock

    db::ForBots(database, /*TODO: (config.concurrency & config_t::Worker) != 0, */ [&](db::bit_int id, db::integer program_id, cell pos, Rotation rot) {
        if (Bot *bot = getBot(id, program_id)) {
            auto &ctx = bot->ctx();
            ctx.position = pos;
            ctx.rotation = rot;
            loadChunksAround(chunk_pos::From(pos, config.chunk_scale), ctx.chunks);

            //MAYBE: job
            if (auto err = bot->tick(tick_ops)) {
                // MAYBE: error penalty
                spdlog::warn("Bot tick error {}({}): {}", id, program_id, *err);
                const uint idx = ctx.strings.size();
                ctx.strings.emplace_back(std::move(*err));
                ctx.actions.emplace_back(action::crash{idx});
            }
            if (procedural)
                applyChanges(ctx);
        }
    });

    if (!procedural) {
        for (const auto& it: cache.bots) {
            if (it.second)
               applyChanges(it.second->ctx());
        }
    }
}

void Engine::loadChunksAround(chunk_pos p, chunk_surrounding& out) {
    //NOTE: not thread safe

    for (int x = -surrounding_radius; x <= surrounding_radius; x++) {
    for (int y = -surrounding_radius; y <= surrounding_radius; y++) {
        At(out, x, y) = [&](chunk_pos ck) -> const chunk_cells * {
            if (const auto it = cache.chunks.find(ck); it != cache.chunks.end())
                return &it->second;

            const auto area = area::From(ck, config.chunk_scale);
            int n_wait = 30;
            while(true) {
                if (const auto generated = db::FindChunk(database, ck)) {
                    if (!generated.value()) {
                        if (n_wait > 0) {
                            n_wait--;
                            spdlog::trace("Waiting chunk {} ({})", ck, n_wait);
                            std::this_thread::sleep_for(std::chrono::milliseconds(100));
                        } else {
                            spdlog::error("Generation of chunk {} probably failed: retrying", ck);
                            db::RetryChunk(database, ck);
                        }
                        continue;
                    }

                    auto cells = db::GetCells(database, area);
                    const auto it = cache.chunks.emplace(ck, std::move(cells));
                    assert(it.second);
                    return &it.first->second;
                }

                if (db::AddChunk(database, ck)) {
                    spdlog::debug("Generate chunk {}", ck);
                    const auto size = chunk_pos::Size(config.chunk_scale);
                    FastNoiseLite noise; //MAYBE: update to FastNoise2
                    noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2);
                    noise.SetSeed(config.seed);
                    const auto tnx = database->transaction();
                    const auto n_generated = db::WriteCells(tnx,
                    [&](const db::write_cell_fn& add) {
                        for (int x = 0; x < size; x++) {
                        for (int y = 0; y < size; y++) {
                            const auto pos = area.min + cell{x, y};
                            if (noise.GetNoise((float)pos.x, (float)pos.y) < 0) {
                                add(pos);
                            }
                        }}
                    });
                    db::FinishChunk(tnx, ck);
                    tnx->commit();
                    db::RequestSnapshot(database, n_generated);
                }
            }
        }(p + chunk_pos{x, y});
    }
    }
}

template<class... Ts> struct make_visitor: Ts... { using Ts::operator()...; };
template<class... Ts> make_visitor(Ts...) -> make_visitor<Ts...>;

void Engine::applyChanges(Bot::context_t& ctx) {
    //NOTE: not thread safe

    const auto str = [&](action::str_ref r) { return ctx.strings.at(r.idx); };
    for (const auto &action : ctx.actions) {
        std::visit(make_visitor {
            [&](const action::move& move) {
                //NOTE: only handle 1x1 bots
                if (move.dist <= 0) {
                    //TODO: what to do ? (trap during add or go backward)
                    db::EmitEvent(database, EventType::Log, {
                        {"target", "bot"}, {"id", std::to_string(ctx.id)}, {"level", "warn"},
                        {"kind", "bad_move"}, {"message", "can not go backward"}
                    });
                    return;
                }

                const auto tnx = [&] {
                    if (config.concurrency == config_t::Procedural) {
                        return database->direct();
                    } else {
                        const auto tnx = database->transaction(tao::pq::isolation_level::serializable);
                        const auto [pos, rot] = db::GetBotPos(tnx, ctx.id);
                        ctx.rotation = rot;
                        ctx.position = pos;
                        return tnx;
                    }
                }();

                const auto dir = Direction(ctx.rotation);
                const auto range = [move] (cell pos, Rotation rot) {
                    auto range = area{pos, pos};
                    switch (rot) {
                        case Rotation::Up:
                            range.max.y += move.dist;
                            break;
                        case Rotation::Right:
                            range.max.x += move.dist;
                            break;
                        case Rotation::Down:
                            range.min.y -= move.dist;
                            break;
                        case Rotation::Left:
                            range.min.x -= move.dist;
                            break;
                        default:
                            break;
                    }
                    return range;
                }(ctx.position, ctx.rotation);
                //NOTE: suppose generated chunks
                const auto cells = db::GetCells(tnx, range);
                assert(cells.at(ctx.position).id == ctx.id);

                const cell from = ctx.position;
                for (coord_t i = 1; i <= move.dist; i++) {
                    const auto target = ctx.position + dir;
                    if (cells.find(target) != cells.end())
                        break;

                    ctx.position = target;
                }
                if (ctx.position != from) {
                    db::MovePointEntity(tnx, ctx.id, ctx.position);
                    if (config.concurrency == config_t::Procedural) {
                        // Update chunk cache
                        auto it_c = cache.chunks.find(chunk_pos::From(from, config.chunk_scale));
                        if (it_c != cache.chunks.end()) {
                            const auto it = it_c->second.find(from);
                            if (it != it_c->second.end()) {
                                assert(it->second.id == ctx.id);
                                it_c->second.erase(it);
                            }
                        }
                        if (const auto ck = chunk_pos::From(ctx.position, config.chunk_scale);
                            it_c == cache.chunks.end() || it_c->first != ck) {
                            it_c = cache.chunks.find(ck);
                        }
                        if (it_c != cache.chunks.end()) {
                            [[maybe_unused]]
                            const auto ok = it_c->second.emplace(ctx.position,
                                entity{ctx.id, EntityType::Bot}).second;
                            assert(ok);
                        }
                    }
                    db::EmitEvent(tnx, EventType::Move, {
                        {"id", std::to_string(ctx.id)}, {"position", {
                        {"x", ctx.position.x}, {"y", ctx.position.y}}}
                    });
                }
                tnx->commit();
            },
            [&](const action::rotate& rot) {
                //NOTE: only handle nxn bots
                ctx.rotation = db::RotateBot(database, ctx.id, rot.left);
                db::EmitEvent(database, EventType::Rotate, {
                    {"id", std::to_string(ctx.id)}, {"rotation", (unsigned char)ctx.rotation}
                });
            },
            [&](const action::log& log) {
                db::EmitEvent(database, EventType::Log, {
                    {"target", "bot"}, {"id", std::to_string(ctx.id)}, {"level", "info"},
                    {"kind", "console"}, {"message", str(log).c_str()}
                });
            },
            [&](const action::crash& err) {
                db::EmitEvent(database, EventType::Log, {
                    {"target", "bot"}, {"id", std::to_string(ctx.id)}, {"level", "error"},
                    {"kind", "crash"}, {"program_id", ctx.program_id},
                    {"message", str(err).c_str()}
                });
            }
        }, action);
    }
    ctx.actions.clear();
    ctx.strings.clear();
}

Bot *Engine::getBot(db::bit_int id, db::integer program_id) {
    if (const auto it = cache.bots.find(id); it != cache.bots.end()) {
        if (it->second->ctx().program_id == program_id)
            return it->second.get();
        else
            cache.bots.erase(it);
    }

    auto *const program = [&]() -> wasm::Program * {
        if (const auto it = cache.programs.find(id); it != cache.programs.end()) {
            //MAYBE: delete if program change
            return it->second.get();
        }

        const auto res = db::Program(database, program_id);
        const auto row = res.at(0);

        const auto checked = !row.is_null(1);
        if (checked && *row.get(1) != '\0') {
            //MAYBE: ignore if program change
            return nullptr;
        }

        const auto code = row.get<tao::pq::bytea>(0);
        auto bld = sandbox.newModule({code.data(), code.size()});
        if (!checked) {
            db::SetProgramResult(database, program_id, bld.isError().value_or(""));
        }

        if (bld.isOk()) {
            spdlog::trace("Program loaded {}", program_id);
            return cache.programs.emplace(id, bld.buildPtr())
                .first->second.get();
        }

        spdlog::warn("Program error {}: {}", program_id, bld.getError());
        db::EmitEvent(database, EventType::Log, {
            {"target", "program"}, {"id", program_id}, {"level", "error"},
            {"message", bld.getError()}
        });
        return nullptr;
    }();
    if (!program) {
        db::EmitEvent(database, EventType::Log, {
            {"target", "bot"}, {"id", std::to_string(id)}, {"level", "error"},
            {"kind", "bad_program"}, {"program_id", program_id}
        });
        return nullptr;
    }

    auto ctx = std::make_unique<Bot::context_t>(); {
        ctx->id = id;
        ctx->program_id = program_id;
        ctx->chunk_scale = config.chunk_scale;
    }

    std::string err;
    auto bot = newBot(*program, std::move(ctx), init_ops, err);
    if (!bot) {
        spdlog::warn("Bot boot error {}({}): {}", id, program_id, err);
        db::EmitEvent(database, EventType::Log, {
            {"target", "bot"}, {"id", std::to_string(id)}, {"level", "error"},
            {"kind", "boot_loop"}, {"program_id", program_id},
            {"message", err}
        });
        return nullptr;
    }

    spdlog::trace("Bot loaded {}", id);
    return cache.bots.emplace(id, std::move(bot))
        .first->second.get();
}

std::unique_ptr<Bot> Engine::newBot(wasm::Program& p, std::unique_ptr<Bot::context_t>&& ctx, uint64_t init_ops, std::string& out) {
    auto linker = sandbox.newLinker();
    ctx->store = linker.getStore();
    if (auto err = API::Setup(linker, ctx.get())) {
        out = std::move(*err);
        return nullptr;
    }

    auto bld = p.newInstance(std::move(linker), init_ops);
    if (auto& err = bld.isError()) {
        out = std::move(*err);
        return nullptr;
    }

    return std::make_unique<Bot>(std::move(bld.build()), std::move(ctx));
}
