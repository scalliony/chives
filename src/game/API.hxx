#include "Bot.hpp"
#include "../wasm/Linker.hxx"

namespace game::API {

/// Fake pointer deleter
void no_deleter(void*) { }

using ctx = Bot::context_t;
/// Produce trap error
inline wasm_trap_t* make_err(void* env, const char* msg) {
    wasm::name err(msg);
    return wasm_trap_new(((ctx*)env)->store, &err);
}
/// Produce not trap aka success
inline constexpr wasm_trap_t *make_ok() { return NULL; }

/// Get access to caller memory
template<typename F>
inline wasm_trap_t* with_memory(const wasmtime_caller_t* caller, void* env, F f) {
    wasm::name mem_name("memory");
    wasm_extern_t *mem_ext = wasmtime_caller_export_get(caller, &mem_name);
    if (mem_ext == NULL || wasm_extern_kind(mem_ext) != WASM_EXTERN_MEMORY)
        return make_err(env, "Failed to find host memory");

    wasm_trap_t* res = f(wasm_extern_as_memory(mem_ext));
    wasm_extern_delete(mem_ext);
    return res;
}

/// Get access to the address of a return pointer in caller memory
template<std::size_t Arg, std::size_t Len, typename F>
inline wasm_trap_t* with_memory_out(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, F f) {
    return with_memory(caller, env, [&](wasm_memory_t *mem) {
        assert(args->size > Arg);
        const auto ptr = args->data[Arg].of.i32;
        if ((size_t)(ptr + Len) > wasm_memory_data_size(mem))
            return make_err(env, "Return pointer out of bounds");

        return f(wasm_memory_data(mem) + ptr);
    });
}

wasm_trap_t* api_console_log_cb(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, wasm_val_vec_t*) {
    return with_memory(caller, env, [&](wasm_memory_t *mem) {
        const auto ptr = args->data[0].of.i32; //FIXME: reinterpret unsigned ?
        const auto len = args->data[1].of.i32;
        if ((size_t)(ptr + len) > wasm_memory_data_size(mem))
            return make_err(env, "String out of bounds");

        const auto data = wasm_memory_data(mem) + ptr;
        if (!utf8::is_valid(data, len))
            return make_err(env, "Invalid utf8");

        const auto context = (ctx*)env;
        const uint idx = context->strings.size();
        context->strings.emplace_back(data, len);
        context->actions.emplace_back(action::log{idx});
        return make_ok();
    });
}

wasm_trap_t* api_env_chunk_scale_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    ret->data[0].of.i32 = ((ctx*)env)->chunk_scale;
    return make_ok();
}
wasm_trap_t* api_env_bot_id_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    ret->data[0].of.i64 = ((ctx*)env)->id;
    return make_ok();
}
wasm_trap_t* api_env_program_id_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    ret->data[0].of.i32 = ((ctx*)env)->program_id;
    return make_ok();
}

wasm_trap_t* api_motor_rotate_cb(void* env, const wasm_val_vec_t *args, wasm_val_vec_t*) {
    const auto left = args->data[0].of.i32 < 0;
    const auto context = (ctx*)env;
    context->actions.emplace_back(action::rotate{left});
    context->rotation = Rotated(context->rotation, left ? -1 : 1);
    return make_ok();
}
wasm_trap_t* api_motor_move_cb(void* env, const wasm_val_vec_t *args, wasm_val_vec_t*) {
    const auto dist = args->data[0].of.i32;
    const auto context = (ctx*)env;
    context->actions.emplace_back(action::move{dist});
    return make_ok();
}

template<typename F>
inline wasm_trap_t* with_cell(void* env, cell target, F f) {
    const auto context = (ctx*)env;
    const auto ck_rel = chunk_pos::From(target, context->chunk_scale)
        - chunk_pos::From(context->position, context->chunk_scale);
    if (!InSurrounding(ck_rel.x, ck_rel.y))
        return make_err(env, "Cell position out of range");

    const auto ck = At(context->chunks, ck_rel.x, ck_rel.y);
    const auto it = ck->find(target);
    return f(it != ck->end() ? std::make_optional(it->second) : std::nullopt);
}
inline void return_entity(const std::optional<entity>& e, wasm_val_vec_t *ret, size_t off = 0) {
    assert(ret->size >= off + 2);
    ret->data[off].of.i64 = e ? e->id : -1;
    ret->data[off+1].of.i32 = e ? (int32_t)e->type : -1;
}
inline void return_entity(const std::optional<entity>& e, byte_t *ptr) {
    *(uint64_t*)ptr = e ? e->id : -1;
    *(uint32_t*)(ptr+sizeof(e->id)) = e ? (int32_t)e->type : -1;
}
constexpr auto entity_len = sizeof(int64_t)+sizeof(int32_t);

template<typename R>
inline wasm_trap_t* api_sensors_contact(void* env, R *ret) {
    const auto context = (ctx*)env;
    const auto target = context->position + Direction(context->rotation);
    return with_cell(env, target, [&](std::optional<entity> cell) {
        return_entity(cell, ret);
        return make_ok();
    });
}
wasm_trap_t* api_sensors_contact_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    return api_sensors_contact(env, ret);
}
wasm_trap_t* api_sensors_contact_s_cb(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, wasm_val_vec_t *) {
    return with_memory_out<0, entity_len>(caller, env, args, [&](byte_t *ptr) {
        return api_sensors_contact(env, ptr);
    });
}

template<typename R>
inline wasm_trap_t* api_sensors_radar(void* env, const wasm_val_vec_t *args, R *ret) {
    const auto target = ((ctx*)env)->position + cell{args->data[0].of.i32, args->data[1].of.i32};
    return with_cell(env, target, [&](std::optional<entity> cell) {
        return_entity(cell, ret);
        return make_ok();
    });
}
wasm_trap_t* api_sensors_radar_cb(void* env, const wasm_val_vec_t *args, wasm_val_vec_t *ret) {
    return api_sensors_radar(env, args, ret);
}
wasm_trap_t* api_sensors_radar_s_cb(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, wasm_val_vec_t *) {
    return with_memory_out<2, entity_len>(caller, env, args, [&](byte_t *ptr) {
        return api_sensors_radar(env, args, ptr);
    });
}

template<typename R>
inline wasm_trap_t* api_sensors_map(void* env, const wasm_val_vec_t *args, R *ret) {
    const auto target = cell{args->data[0].of.i32, args->data[1].of.i32};
    return with_cell(env, target, [&](std::optional<entity> cell) {
        return_entity(cell, ret);
        return make_ok();
    });
}
wasm_trap_t* api_sensors_map_cb(void* env, const wasm_val_vec_t *args, wasm_val_vec_t *ret) {
    return api_sensors_map(env, args, ret);
}
wasm_trap_t* api_sensors_map_s_cb(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, wasm_val_vec_t *) {
    return with_memory_out<2, entity_len>(caller, env, args, [&](byte_t *ptr) {
        return api_sensors_map(env, args, ptr);
    });
}

wasm_trap_t* api_sensors_gyroscope_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    ret->data[0].of.i32 = (int)((ctx*)env)->rotation;
    return make_ok();
}

wasm_trap_t* api_sensors_gps_cb(void* env, const wasm_val_vec_t *, wasm_val_vec_t *ret) {
    const auto p = ((ctx*)env)->position;
    ret->data[0].of.i32 = p.x;
    ret->data[1].of.i32 = p.y;
    return make_ok();
}
wasm_trap_t* api_sensors_gps_s_cb(const wasmtime_caller_t* caller, void* env, const wasm_val_vec_t *args, wasm_val_vec_t *) {
    return with_memory_out<0, sizeof(game::cell)>(caller, env, args, [&](byte_t *ptr) {
        const auto p = ((ctx*)env)->position;
        *ptr = p.x;
        *(ptr + sizeof(p.x)) = p.y;
        return make_ok();
    });
}

wasm::result_t Setup(wasm::Linker& api, ctx* ctx) {
    using namespace wasm;
    {
        name console_name("console");
        Linker::functype str_type(valkinds<2>{WASM_I32, WASM_I32}, valkinds<0>());
        if (auto err = api.define(console_name, "log", str_type, api_console_log_cb, ctx, no_deleter))
            return err;
    }
    {
        name env_name("env");
        Linker::functype i32_type(valkinds<0>(), valkinds<1>{WASM_I32});
        Linker::functype i64_type(valkinds<0>(), valkinds<1>{WASM_I64});
        if (auto err = api.define(env_name, "chunk_scale", i32_type, api_env_chunk_scale_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(env_name, "bot_id", i64_type, api_env_bot_id_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(env_name, "program_id", i32_type, api_env_program_id_cb, ctx, no_deleter))
            return err;
    }
    {
        name motor_name("motor");
        Linker::functype one_type(valkinds<1>{WASM_I32}, valkinds<0>());
        if (auto err = api.define(motor_name, "rotate", one_type, api_motor_rotate_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(motor_name, "move", one_type, api_motor_move_cb, ctx, no_deleter))
            return err;
    }
    {
        name sensors_name("sensors");
        Linker::functype get_cell_type(valkinds<0>(), valkinds<2>{WASM_I64, WASM_I32});
        Linker::functype get_cell_type_single(valkinds<1>{WASM_I32}, valkinds<0>());
        if (auto err = api.define(sensors_name, "contact", get_cell_type, api_sensors_contact_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(sensors_name, "contact_s", get_cell_type_single, api_sensors_contact_s_cb, ctx, no_deleter))
            return err;
        Linker::functype query_cell_type(valkinds<2>{WASM_I32, WASM_I32}, valkinds<2>{WASM_I64, WASM_I32});
        Linker::functype query_cell_single_type(valkinds<3>{WASM_I32, WASM_I32, WASM_I32}, valkinds<0>());
        if (auto err = api.define(sensors_name, "radar", query_cell_type, api_sensors_radar_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(sensors_name, "radar_s", query_cell_single_type, api_sensors_radar_s_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(sensors_name, "map", query_cell_type, api_sensors_map_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(sensors_name, "map_s", query_cell_single_type, api_sensors_map_s_cb, ctx, no_deleter))
            return err;
        Linker::functype get_rot_type(valkinds<0>(), valkinds<1>{WASM_I32});
        if (auto err = api.define(sensors_name, "gyroscope", get_rot_type, api_sensors_gyroscope_cb, ctx, no_deleter))
            return err;
        Linker::functype get_pos_type(valkinds<0>(), valkinds<2>{WASM_I32, WASM_I32});
        Linker::functype get_pos_single_type(valkinds<1>{WASM_I32}, valkinds<0>());
        if (auto err = api.define(sensors_name, "gps", get_pos_type, api_sensors_gps_cb, ctx, no_deleter))
            return err;
        if (auto err = api.define(sensors_name, "gps_s", get_pos_single_type, api_sensors_gps_s_cb, ctx, no_deleter))
            return err;
    }
    return {};
}

}