#pragma once

#include "../wasm/Sandbox.hpp"
#include "types.hpp"

namespace game {
class Bot;
struct bot_context_t;

/** Game core */
class Engine {
public:
    Engine(db::connection&&);
    ~Engine();

    void run();

    static const char** concurrency_names;

private:
    void tick();

    void loadChunksAround(chunk_pos, chunk_surrounding& out);

    Bot *getBot(db::bit_int id, db::integer program_id);
    std::unique_ptr<Bot> newBot(wasm::Program&, std::unique_ptr<bot_context_t>&&, uint64_t init_ops, std::string& err);

    void applyChanges(bot_context_t&);

    db::connection database;
    struct config_t {
        uint16_t chunk_scale;
        float tick_per_second;
        size_t snapshot_limit;
        int seed;
        enum concurrency_t : unsigned char {
            Procedural = 0,
            Thread,
            Process,
            Full = Thread | Process
        } concurrency = Process;
    } config;

    wasm::Core sandbox;
    struct cache_t {
        std::unordered_map<chunk_pos, chunk_cells> chunks;
        std::unordered_map<db::integer, std::unique_ptr<wasm::Program>> programs;
        std::unordered_map<db::bit_int, std::unique_ptr<Bot>> bots;
    } cache;
};
}