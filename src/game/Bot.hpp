#pragma once

#include "types.hpp"
#include "../wasm/Sandbox.hpp"
#include "utils/utf8.h"

namespace game {

/** All informations ~shared with runner */
struct bot_context_t {
    /* const */
    int64_t id;
    int32_t program_id;
    uint16_t chunk_scale;
    wasm_store_t *store;

    /* user read */
    cell position;
    Rotation rotation;
    /// Chunks of size chunk_scale around position
    chunk_surrounding chunks;

    /* user write */
    std::vector<action::any> actions;
    std::vector<utf8::string> strings;
};

/** In-game bot instance */
class Bot {
public:
    ~Bot() {}

    using context_t = bot_context_t;
    wasm::result_t tick(uint64_t min_ops);
    inline context_t &ctx() { return *context.get(); }

    Bot(wasm::Instance&&, std::unique_ptr<context_t>&&);
private:
    wasm::Instance runner;
    std::unique_ptr<context_t> context;
};

}