#include "Bot.hpp"

using namespace game;

Bot::Bot(wasm::Instance&& i, std::unique_ptr<context_t>&& c):
    runner(std::move(i)), context(std::move(c)) { }

wasm::result_t Bot::tick(uint64_t min_ops) {
    return runner.run(min_ops);
}