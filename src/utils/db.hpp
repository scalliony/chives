#pragma once

#include <tao/pq.hpp>
namespace db {
    using connection = std::shared_ptr<tao::pq::connection>;
    using result = tao::pq::result;
    using row = tao::pq::row;
    using field = tao::pq::field;
    using integer = int32_t;
    using bit_int = int64_t;
}