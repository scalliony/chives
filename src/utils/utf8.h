#pragma once

#include <tinyutf8/tinyutf8.h>
namespace utf8 {
    bool is_valid(const char*, size_t);
    using string = tiny_utf8::string;
}