#include <iostream>
#include "game/Engine.hpp"
#include <spdlog/cfg/env.h>

int main(int argc, char const *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage %s <postgresql://...>" << std::endl;
        return 1;
    }

    try {
        spdlog::cfg::load_env_levels();
        auto db = tao::pq::connection::create(argv[1]);
        game::Engine engine(std::move(db));
        engine.run();
    }
    catch(const std::exception& e) {
        std::cerr << e.what() << '\n';
        return 1;
    }
    return 0;
}
