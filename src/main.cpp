#include <CLI11.hpp>
#include <iostream>
#include "game/Engine.hpp"
#include <spdlog/cfg/env.h>
#include <version.h>
#include <schema.h>

namespace std {
    std::string to_string(const std::string &v) { return v; }
}

int main(int argc, char **argv) {

    spdlog::cfg::load_env_levels();

    CLI::App app("Scalliony game engine v" CHIVES_VERSION);
    app.set_help_all_flag("--help-all", "Expand all help");
    app.set_help_flag("--help", "Display help");

    std::string db_uri;
    app.add_option("database", db_uri, "Database url (postgresql://...)")->required();

    CLI::App *init = app.add_subcommand("init", "Initialize shard database");
    bool non_interactive = false;
    init->add_flag("--non-interactive,-y", non_interactive, "Accept all default options");
    bool init_try = false;
    init->add_flag("--try", init_try, "Just skip init if database is allready populated");
    std::map<std::string, std::string> init_config;
    init->add_option("--tick_per_second", init_config["tick_per_second"], "Override default config option");
    bool init_sample = false;
    init->add_flag("--sample", init_sample, "Load sample data");
    CLI::App *worker = app.add_subcommand("worker", "Add worker to shard");
    worker->alias("run");
    app.require_subcommand(1, 2);

    init->callback([&]() {
        try {
            auto db = tao::pq::connection::create(db_uri);
            { // Check empty
                auto res = db->execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'");
                if (!res.empty()) {
                    const auto err = fmt::format("Database is not empty: found {} tables", res.size());
                    if (init_try) {
                        std::cout << err << ". skip init" << std::endl;
                        return;
                    }
                    throw std::runtime_error(err);
                }
                std::cout << "Initializing schema" << std::endl;
                db::RunScript(db, CHIVES_SQL_SCHEMA);
                db->prepare("cfg", "INSERT INTO config(key, value) VALUES ($1, $2)");
                const auto cfg = [&](const std::string &key, const std::string &val) {
                    db->execute("cfg", key, val);
                };
                std::cout << "Defining configuration" << std::endl;
                cfg("version", CHIVES_VERSION);
                const auto getCfg = [&](const std::string &key, const std::string &desc, auto parse, auto base) {
                    if (const auto it = init_config.find(key); it != init_config.end() && !it->second.empty()) {
                        base = parse(it->second);
                    }
                    std::string input;
                    std::cout << desc << " (" << key << ")[default: " << base << "]" << std::endl;
                    if (!non_interactive) {
                        std::getline(std::cin, input);
                    }
                    cfg(key, std::to_string(input.empty() ? base : parse(input)));
                };
                const auto toInt = [](const std::string &s) { return std::stoi(s); };
                const auto toFloat = [](const std::string &s) { return std::stof(s); };
                getCfg("chunk_scale", "Chunk size power of 2", toInt, 6);
                getCfg("seed", "Random generation initializer", toInt, 42);
                getCfg("tick_per_second", "Updates per second", toFloat, 1.f);
                getCfg("snapshot_limit", "Modifications between each snapshot", toInt, 10000);
                cfg("snapshot_inc", std::string("0"));
                getCfg("concurrency", "Workers parallelism model", [](const std::string& s) {
                    for (size_t i = 0; game::Engine::concurrency_names[i] != NULL; i++) {
                        if (strcmp(s.c_str(), game::Engine::concurrency_names[i]) == 0) {
                            return s;
                        }
                    }
                    throw std::runtime_error("Invalid concurrency level.\nSee game::Engine::concurrency_names");
                }, std::string("procedural"));
                cfg("worker_count", std::string("0"));
                if (init_sample) {
                    std::cout << "Loading sample" << std::endl;
                    db::RunScript(db, CHIVES_SQL_SAMPLE);
                }
                std::cout << "Done" << std::endl;
            }
        }
        catch(const std::exception& e) {
            std::cerr << e.what() << '\n';
            exit(1);
        }
    });
    worker->callback([&]() {
        try {
            auto db = tao::pq::connection::create(db_uri);
            game::Engine engine(std::move(db));
            engine.run();
        }
        catch(const std::exception& e) {
            std::cerr << e.what() << '\n';
            exit(1);
        }
    });

    CLI11_PARSE(app, argc, argv);
    return 0;
}
