#pragma once

#include <optional>
#include <string>
#include <memory>

struct wasm_engine_t;
struct wasm_store_t;
struct wasm_module_t;
struct wasm_instance_t;
struct wasm_byte_vec_t;
struct wasmtime_error_t;
struct wasm_trap_t;
struct wasm_extern_t;
struct wasm_extern_vec_t;
struct wasm_func_t;
struct wasm_val_vec_t;
struct wasmtime_linker_t;

namespace wasm {
    using result_t = std::optional<std::string>;
    template<typename T>
    using deleted_unique_ptr = std::unique_ptr<T,void(*)(T*)>;
    class Linker;
}