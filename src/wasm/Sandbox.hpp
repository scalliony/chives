#pragma once

#include "fwd.h"

namespace wasm {

class Memory {
public:
    Memory(wasm_store_t*);

    void addOps(uint64_t);
    void setOpsMin(uint64_t);
    uint64_t totalUsedOps() const;
    uint64_t cycleUsedOps() const;
    uint64_t remainingOps() const;

private:
    deleted_unique_ptr<wasm_store_t> self;

    uint64_t total_ops = 0;
    uint64_t last_used_ops = 0;
};

class Instance {
public:
    class Builder {
    public:
        Builder(const std::string &error);
        Builder(wasm_instance_t*, wasm_module_t*, Memory &&);

        const result_t &isError() const { return error; }
        bool isOk() const { return !isError(); }
        const std::string &getError() const { return error.value(); }
        Instance build();

    private:
        Memory mem;
        deleted_unique_ptr<wasm_instance_t> self;
        wasm_module_t* module;
        result_t error;

    friend class Instance;
    };

    result_t run(uint64_t min_ops);

    const Memory& memory() const { return mem; }

private:
    Instance(wasm_instance_t*, wasm_module_t*, Memory &&);
    Memory mem;
    deleted_unique_ptr<wasm_instance_t> self;
    wasm_module_t* module;
};

class Program {
public:
    class Builder {
    public:
        Builder(wasm_module_t*);
        Builder(const std::string &error);

        const result_t &isError() const { return error; }
        bool isOk() const { return !isError(); }
        const std::string &getError() const { return error.value(); }
        Program build();
        std::unique_ptr<Program> buildPtr();

    private:
        deleted_unique_ptr<wasm_module_t> self;
        result_t error;

    friend class Program;
    };

    Instance::Builder newInstance(Linker&&, uint64_t max_ops);

private:
    Program(wasm_module_t*);
    deleted_unique_ptr<wasm_module_t> self;
};

struct code_t {
    const unsigned char *data;
    size_t size;
};

class Core {
public:
    Core();

    Program::Builder newModule(code_t);
    Linker newLinker();

private:
    deleted_unique_ptr<wasm_engine_t> self;
};

}