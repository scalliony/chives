#pragma once

#include "fwd.h"
#include <wasm.h>
#include <wasmtime.h>
#include <spdlog/spdlog.h>

#include <iostream>
namespace wasm {

inline std::string write_error(const char *message, wasmtime_error_t* error) {
    wasm_byte_vec_t error_message;
    wasmtime_error_message(error, &error_message);
    wasmtime_error_delete(error);
    auto str = fmt::format("{}: {}", message, fmt::basic_string_view(error_message.data, error_message.size));
    wasm_byte_vec_delete(&error_message);
    return str;
}
inline std::string write_failure(const char *message, wasmtime_error_t* error, wasm_trap_t* trap) {
    wasm_byte_vec_t error_message;
    if (error != NULL) {
        wasmtime_error_message(error, &error_message);
        wasmtime_error_delete(error);
    } else {
        assert(trap != NULL);
        wasm_trap_message(trap, &error_message);
        wasm_trap_delete(trap);
    }
    auto str = fmt::format("{}: {}", message, fmt::basic_string_view(error_message.data, error_message.size));
    wasm_byte_vec_delete(&error_message);
    return str;
}

template <std::size_t N>
using valkinds = std::array<wasm_valkind_t, N>;

struct name final: public wasm_name_t {
    name(const char* v) {
        wasm_name_new_from_string(this, v);
    }
    ~name() {
        wasm_name_delete(this);
    }
};

class Linker {
public:
    Linker(wasmtime_linker_t* l, wasm_store_t* s): self(l), store(s) { }
    Linker(const Linker &) = delete;
    Linker &operator=(const Linker &) = delete;
    Linker(Linker&& other) noexcept: Linker(other.self, other.store) {
        fns.swap(other.fns);
        other.self = NULL;
    }
    Linker& operator=(Linker&& other) noexcept {
        if (self != NULL) {
            wasmtime_linker_delete(self);
        }
        self = other.self;
        store = other.store;
        fns.swap(other.fns);
        other.self = NULL;
        return *this;
    }
    ~Linker() {
        for(auto fn: fns) {
            wasm_func_delete(fn);
        }
        fns.clear();
        if (self != NULL) {
            wasmtime_linker_delete(self);
            self = NULL;
        }
    }

    result_t define(const name& module, const name& name, wasm_extern_t* item) {
        wasmtime_error_t *error = wasmtime_linker_define(self, &module, &name, item);
        if (error != NULL)
            return write_error("Failed to setup Linker", error);

        return {};
    }
    result_t define(const name& module, const name& name, wasm_func_t* fn) {
        return define(module, name, wasm_func_as_extern(fn));
    }

    result_t newInstance(wasm_module_t* module, wasm_instance_t**instance) {
        wasm_trap_t *trap = NULL;
        wasmtime_error_t *error = wasmtime_linker_instantiate(self, module, instance, &trap);
        if (error != NULL || trap != NULL) {
            return write_failure("Failed to instantiate program", error, trap);
        }
        return {};
    }
    wasm_store_t *getStore() { return store; }

    struct functype {
        ~functype() {
            wasm_functype_delete(self);
        }
        wasm_functype_t* self;

        template <std::size_t NP, std::size_t NR>
        functype(const valkinds<NP>& ps, const valkinds<NR>& rs) {
            wasm_valtype_vec_t params, results;
            if constexpr (NP != 0) {
                wasm_valtype_vec_new_uninitialized(&params, NP);
                for (size_t i = 0; i < NP; i++) {
                    params.data[i] = wasm_valtype_new(ps[i]);
                }
            } else {
                wasm_valtype_vec_new_empty(&params);
            }
            if constexpr (NR != 0) {
                wasm_valtype_vec_new_uninitialized(&results, NR);
                for (size_t i = 0; i < NR; i++) {
                    results.data[i] = wasm_valtype_new(rs[i]);
                }
            } else {
                wasm_valtype_vec_new_empty(&results);
            }
            self = wasm_functype_new(&params, &results);
        }
    };

    result_t define(const name& module, const name& name, const functype& typ, wasm_func_callback_t cb) {
        auto fn = wasm_func_new(store, typ.self, cb);
        return define(module, name, fns.emplace_back(fn));
    }
    result_t define(const name& module, const name& name, const functype& typ, wasm_func_callback_with_env_t cb, void* env, void(*deleter)(void*)) {
        auto fn = wasm_func_new_with_env(store, typ.self, cb, env, deleter);
        return define(module, name, fns.emplace_back(fn));
    }
    result_t define(const name& module, const name& name, const functype& typ, wasmtime_func_callback_t cb) {
        auto fn = wasmtime_func_new(store, typ.self, cb);
        return define(module, name, fns.emplace_back(fn));
    }
    result_t define(const name& module, const name& name, const functype& typ, wasmtime_func_callback_with_env_t cb, void* env, void(*deleter)(void*)) {
        auto fn = wasmtime_func_new_with_env(store, typ.self, cb, env, deleter);
        return define(module, name, fns.emplace_back(fn));
    }

    Memory newMemory() {
        return store;
    }

private:
    wasmtime_linker_t* self;
    wasm_store_t* store;
    std::vector<wasm_func_t *> fns;
};
}