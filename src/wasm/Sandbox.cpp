#include "Sandbox.hpp"

#include "Linker.hxx"
#include "utils/utf8.h"

using namespace wasm;

void log_error(const char *message, wasmtime_error_t* error) {
    spdlog::error("WASM fatal error {}", write_error(message, error));
}
void fatal_error(const char *message, wasmtime_error_t* error) {
    if (error != NULL) {
        log_error(message, error);
        exit(1);
    }
}

template <typename T>
void no_deleter(T*){ }

Core::Core(): self(NULL, no_deleter) {
    wasm_config_t* config = wasm_config_new();
    assert(config != NULL);
    wasmtime_config_consume_fuel_set(config, true);
    fatal_error("Cannot load cache", wasmtime_config_cache_config_load(config, NULL));

    self = {wasm_engine_new_with_config(config), wasm_engine_delete};
    assert(self);
}

Program::Builder Core::newModule(code_t code) {
    wasm_byte_vec_t wasm;
    if (utf8::is_valid((const char*)code.data, code.size)) {
        wasm_byte_vec_t wat;
        wasm_byte_vec_new_uninitialized(&wat, code.size);
        memcpy(wat.data, code.data, code.size);
        wasmtime_error_t *error = wasmtime_wat2wasm(&wat, &wasm);
        wasm_byte_vec_delete(&wat);
        if (error != NULL) {
            return write_error("Failed to parse code", error);
        }
    } else {
        wasm_byte_vec_new_uninitialized(&wasm, code.size);
        memcpy(wasm.data, code.data, code.size);
    }

    wasm_module_t* module = NULL;
    wasmtime_error_t *error = wasmtime_module_new(self.get(), &wasm, &module);
    wasm_byte_vec_delete(&wasm);
    if (module == NULL) {
        return write_error("Failed to compile program", error);
    }
    return module;
}
Linker Core::newLinker() {
    wasm_store_t* store = wasm_store_new(self.get());
    assert(store != NULL);
    wasmtime_linker_t *linker = wasmtime_linker_new(store);
    assert(linker != NULL);
    return Linker(linker, store);
}

Program::Builder::Builder(wasm_module_t* m): self(m, wasm_module_delete) { }
Program::Builder::Builder(const std::string& e): self(NULL, no_deleter), error(e) { }

Program Program::Builder::build() {
    return self.release();
}
std::unique_ptr<Program> Program::Builder::buildPtr() {
    return std::unique_ptr<Program>(new Program(self.release()));
}
Program::Program(wasm_module_t* m): self(m, wasm_module_delete) { }

struct func {
    wasm_func_t* self;

    wasmtime_error_t *call(const wasm_val_vec_t *params, wasm_val_vec_t *results, wasm_trap_t **failure) {
        return wasmtime_func_call(self, params, results, failure);
    }
    wasmtime_error_t *call(wasm_trap_t **failure) {
        wasm_val_t params[0];
        wasm_val_vec_t params_vec = WASM_ARRAY_VEC(params);
        wasm_val_t results[0];
        wasm_val_vec_t results_vec = WASM_ARRAY_VEC(results);
        return call(&params_vec, &results_vec, failure);
    }
    result_t call(const char* fail_message) {
        wasm_trap_t *trap = NULL;
        wasmtime_error_t *error = call(&trap);
        if (error != NULL || trap != NULL)
            return write_failure(fail_message, error, trap);

        return {};
    }
};

template <std::size_t NP, std::size_t NR, typename F>
void withFunc(wasm_module_t* module, wasm_instance_t* instance, const char* name, const valkinds<NP>& params, const valkinds<NR>& results, F f) {
    assert(module && instance);

    wasm_exporttype_vec_t export_types;
    wasm_extern_vec_t exports;
    wasm_module_exports(module, &export_types);
    wasm_instance_exports(instance, &exports);
    assert(exports.size == export_types.size);

    for (size_t i = 0; i < exports.size; ++i) {
        auto ext = exports.data[i];
        if (wasm_extern_kind(ext) != WASM_EXTERN_FUNC)
            continue;

        assert(wasm_externtype_kind(wasm_exporttype_type(export_types.data[i])) == WASM_EXTERN_FUNC);

        auto fname = wasm_exporttype_name(export_types.data[i]);
        if (strncmp(name, fname->data, fname->size) != 0)
            continue;

        wasm_func_t* fn = wasm_extern_as_func(ext);
        wasm_functype_t* type = wasm_func_type(fn);
        const auto sameValKinds = [] (const wasm_valtype_vec_t* a, const auto& b) {
            if (a->size != b.size())
                return false;

            for (size_t j = 0; j < a->size; j++) {
                if (wasm_valtype_kind(a->data[j]) != b[j])
                    return false;
            }
            return true;
        };
        if (!sameValKinds(wasm_functype_params(type), params))
            continue;

        if (!sameValKinds(wasm_functype_results(type), results))
            continue;

        f(func{fn});
        return;
    }

    wasm_extern_vec_delete(&exports);
    wasm_exporttype_vec_delete(&export_types);
}

Instance::Builder::Builder(wasm_instance_t* i, wasm_module_t* m, Memory && mem): mem(std::move(mem)), self(i, wasm_instance_delete), module(m) { }
Instance::Builder::Builder(const std::string& e): mem(NULL), self(NULL, no_deleter), error(e) { }

Instance Instance::Builder::build() {
    return Instance(self.release(), module, std::move(mem));
}

Instance::Builder Program::newInstance(Linker&& linker, uint64_t max_ops) {
    wasm_instance_t *instance = NULL;
    if (auto err = linker.newInstance(self.get(), &instance))
        return std::move(*err);

    auto memory = linker.newMemory();
    memory.setOpsMin(max_ops);

    result_t err = {};
    withFunc(self.get(), instance, "_start", valkinds<0>(), valkinds<0>(), [&](func fn) {
        err = fn.call("Function '_start'"); });
    if (err)
        return std::move(*err);

    bool hasRun = false;
    withFunc(self.get(), instance, "run", valkinds<0>(), valkinds<0>(), [&](func) { hasRun = true; });
    if (!hasRun)
        return Instance::Builder("Function 'run': not found");

    return Instance::Builder(instance, self.get(), std::move(memory));
}

Instance::Instance(wasm_instance_t* i, wasm_module_t* m, Memory && mem): mem(std::move(mem)), self(i, wasm_instance_delete), module(m) { }

Memory::Memory(wasm_store_t* s): self(s, s ? wasm_store_delete : no_deleter<wasm_store_t>) { }

void Memory::addOps(uint64_t n) {
    total_ops += n;
    wasmtime_store_add_fuel(self.get(), n);
}
void Memory::setOpsMin(uint64_t n) {
    last_used_ops = totalUsedOps();
    const auto remain = remainingOps();
    if (n > remain) {
        addOps(n - remain);
    }
}
uint64_t Memory::totalUsedOps() const {
    uint64_t out = 0;
    wasmtime_store_fuel_consumed(self.get(), &out);
    return out;
}
uint64_t Memory::cycleUsedOps() const {
    return totalUsedOps() - last_used_ops;
}
uint64_t Memory::remainingOps() const {
    return total_ops - totalUsedOps();
}

result_t Instance::run(uint64_t min_ops) {
    mem.setOpsMin(min_ops);

    result_t res = "Function 'run': not found";
    withFunc(module, self.get(), "run", valkinds<0>(), valkinds<0>(), [&](func fn) {
        res = fn.call("Function 'run'"); });

    return res;
}