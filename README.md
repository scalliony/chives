# Chives <!-- omit in toc -->

Scalliony game engine

## Table of Contents <!-- omit in toc -->

- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Run it](#run-it)
  - [Prerequisites](#prerequisites)
  - [Usage](#usage)
  - [Log level](#log-level)
- [Build](#build)
  - [Docker](#docker)
  - [Prerequisites](#prerequisites-1)
    - [Optionally](#optionally)
  - [Installation](#installation)
- [License](#license)


<!-- ABOUT THE PROJECT -->
## About The Project

Opensource RTS programming game using WebAssembly

This project contains background worker which runs game logic

### Built With

* C++
* [wasmtime](https://github.com/bytecodealliance/wasmtime)
* PostgreSQL
* Love and insomnia


<!-- GETTING STARTED -->
## Run it

Get a release compatible with your system, a [container](#docker) or [build](#build) it

### Prerequisites

* Setup postgres server and libpq

### Usage

1. Setup instance
```sh
./chives <postgresql://...> init
```

2. Add workers
```sh
./worker <postgresql://...> # or ./chives <postgresql://...> worker
```
Profit !

*Optionally:* Dynamic setup
```sh
./chives <postgresql:/...> init --non-interactive --try \
[--tick_per_second N] [--sample] run
```


### Log level

This project uses [spdlog](https://github.com/gabime/spdlog), log level can be updated with `SPDLOG_LEVEL` env variable

## Build

To get a local copy up and running, follow these simple steps.

### Docker

From DockerHub
```sh
docker run scalliony/chives ./chives <args...>
```
[Dockerfile](Dockerfile) simply bundles following steps with a multi-stage image.
*No need to manually clone the full repo, copy just this Dockerfile*
```sh
docker build -t custom-chives --build-arg COMMIT=<SHA> - < Dockerfile
docker run custom-chives ./chives <args...>
```

### Prerequisites

* C++17
* CMake 3.11
* Rust toolchain

#### Optionally

* None for now

### Installation

1. Clone the project repo
```sh
git clone --recursive https://framagit.org/scalliony/chives.git
```
2. Create build folder and move
```sh
mkdir build && cd build
```
3. Setup CMake
```sh
cmake <options> ..
```

CMake options: `-DKEY=VAL`

Key | Usage | Default
--- | --- | ---
CMAKE_BUILD_TYPE | Level of optimization | `Release`
NATIVE | Build with -march=native | `0`
IPO | Link time optimisation | `1`
LD_GOLD | Use gold linker | `1`
CCACHE | Use code cache | `1`
STATICXX | Link static libcxx | `0`

1. Compile
```sh
cmake --build . --parallel --target <target>
```

Target | Description
--- | ---
chives | All in one
worker | Worker only

<!-- LICENSE -->
## License

Distributed under the MIT License. See [LICENSE](LICENSE) for more information.