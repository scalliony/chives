FROM rust as prepare
ARG COMMIT=main
RUN git clone --recursive https://framagit.org/scalliony/chives.git
WORKDIR /chives
RUN git checkout ${COMMIT}
RUN apt-get update && apt-get install -y --no-install-recommends cmake build-essential postgresql-all doxygen graphviz
RUN mkdir build
WORKDIR /chives/build
RUN cmake -DSTATICXX=1 .. && cmake --build . --target wasmtime_cargo

FROM gcc as builder
COPY --from=prepare /chives /chives
WORKDIR /chives/build
RUN apt-get update && apt-get install -y --no-install-recommends cmake postgresql-all doxygen graphviz
RUN rm CMakeCache.txt && cmake -DSTATICXX=1 .. && cmake --build . --parallel

FROM buildpack-deps:buster
COPY --from=builder /chives/build/chives .
CMD [ "./chives" ]