CREATE TABLE config (
    key text PRIMARY KEY,
    value text
);

CREATE TABLE player (
    id SERIAL PRIMARY KEY,
    login text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
	CONSTRAINT player_login_uq UNIQUE (login),
    CONSTRAINT player_email_uq UNIQUE (email)
);

CREATE TABLE program (
    id SERIAL PRIMARY KEY,
    owner_id integer NOT NULL references player(id),
    name text NOT NULL,
    data bytea NOT NULL,
    checked text DEFAULT NULL
);

CREATE TYPE entity_type AS ENUM ('rock', 'bot', 'building');
CREATE TABLE entity (
    id BIGSERIAL PRIMARY KEY,
    type entity_type NOT NULL
);

CREATE TABLE bot (
    id bigint PRIMARY KEY references entity(id) ON DELETE CASCADE,
    owner_id integer NOT NULL references player(id),
    program_id integer references program(id),
    rotation smallint NOT NULL DEFAULT 0
);

CREATE TABLE building (
    id bigint PRIMARY KEY references entity(id) ON DELETE CASCADE,
    owner_id integer NOT NULL references player(id)
);

CREATE TABLE cell (
    x integer NOT NULL,
    y integer NOT NULL,
    entity_id bigint NOT NULL references entity(id) ON DELETE CASCADE,
    PRIMARY KEY(x, y)
);
CREATE INDEX cell_entity_id ON cell(entity_id);
CREATE TABLE chunk (
    x integer NOT NULL,
    y integer NOT NULL,
    generated bool NOT NULL DEFAULT false,
    PRIMARY KEY(x, y)
);

CREATE TYPE event_type AS ENUM ('tick', 'move', 'rotate', 'log');
CREATE TABLE event (
    id BIGSERIAL PRIMARY KEY,
    instant timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    type event_type NOT NULL,
    data jsonb NOT NULL
);

-- Notify when record was inserted into 'event' table
CREATE FUNCTION notify_event_inserted()
  RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify(CAST('event' AS text),CAST(NEW.id AS text));
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notify_event_inserted
  AFTER INSERT ON event
  FOR EACH ROW
  EXECUTE PROCEDURE notify_event_inserted();


-- Snapshots
CREATE SCHEMA IF NOT EXISTS snapshot;
CREATE TABLE snapshot.index (
    id SERIAL PRIMARY KEY,
    instant timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    event_id bigint NOT NULL references event(id)
);

CREATE TABLE snapshot.entity (
    snapshot_id integer NOT NULL references snapshot.index(id) ON DELETE CASCADE,
    id BIGSERIAL NOT NULL,
    type entity_type NOT NULL,
    PRIMARY KEY(snapshot_id, id)
);

CREATE TABLE snapshot.bot (
    snapshot_id integer NOT NULL,
    id bigint NOT NULL,
    owner_id integer references player(id) ON DELETE SET NULL,
    program_id integer references program(id) ON DELETE SET NULL,
    rotation smallint NOT NULL DEFAULT 0,
    PRIMARY KEY(snapshot_id, id),
    FOREIGN KEY(snapshot_id, id) references snapshot.entity(snapshot_id, id) ON DELETE CASCADE
);

CREATE TABLE snapshot.building (
    snapshot_id integer NOT NULL,
    id bigint NOT NULL,
    owner_id integer references player(id) ON DELETE SET NULL,
    PRIMARY KEY(snapshot_id, id),
    FOREIGN KEY(snapshot_id, id) references snapshot.entity(snapshot_id, id) ON DELETE CASCADE
);

CREATE TABLE snapshot.cell (
    snapshot_id integer NOT NULL references snapshot.index(id) ON DELETE CASCADE,
    x integer NOT NULL,
    y integer NOT NULL,
    entity_id bigint NOT NULL references entity(id) ON DELETE CASCADE,
    PRIMARY KEY(snapshot_id, x, y)
);

-- Copy state tables into snapshot
CREATE PROCEDURE do_snapshot() AS $$
DECLARE snapshot_id snapshot.index.id%TYPE;
BEGIN
    INSERT INTO snapshot.index (event_id) SELECT MAX(id) FROM event RETURNING id INTO snapshot_id;
    INSERT INTO snapshot.entity SELECT snapshot_id, * FROM entity;
    INSERT INTO snapshot.bot SELECT snapshot_id, * FROM bot;
    INSERT INTO snapshot.building SELECT snapshot_id, * FROM building;
    INSERT INTO snapshot.cell SELECT snapshot_id, * FROM cell;
    PERFORM pg_notify(CAST('snapshot' AS text),CAST(snapshot_id AS text));
END;
$$ LANGUAGE plpgsql;