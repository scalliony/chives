INSERT INTO player(login, email, password) VALUES ('Shu', 'me@wadza.fr', 'x'); -- 1
-- data from api/clang using xxd -p explorer.wasm | tr -d '\n'
INSERT INTO program(owner_id, name, data) VALUES (1, 'explorer.wasm', decode('0061736d01000000010d0360017f0060000060027f7f00023f0407636f6e736f6c65036c6f6700020773656e736f727309636f6e746163745f730000056d6f746f7206726f746174650000056d6f746f72046d6f76650000030302010105030100020608017f01419088040b071903066d656d6f72790200065f737461727400040372756e00050a43020900418008410810000b3701017f230041106b2200240020001001024020002903004201530d002000280208417f460d00417f10020b41031003200041106a24000b0b0f01004180080b085374617274696e6700260970726f647563657273010c70726f6365737365642d62790105636c616e670631312e302e31', 'hex')); -- 1

INSERT INTO entity(type) VALUES ('bot'); -- 1
INSERT INTO cell(x, y, entity_id) VALUES(-1, -1, 1);
INSERT INTO bot(id, owner_id, program_id) VALUES (1, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 2
INSERT INTO cell(x, y, entity_id) VALUES(-1, 0, 2);
INSERT INTO bot(id, owner_id, program_id) VALUES (2, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 3
INSERT INTO cell(x, y, entity_id) VALUES(-1, 1, 3);
INSERT INTO bot(id, owner_id, program_id) VALUES (3, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 4
INSERT INTO cell(x, y, entity_id) VALUES(0, -1, 4);
INSERT INTO bot(id, owner_id, program_id) VALUES (4, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 5
INSERT INTO cell(x, y, entity_id) VALUES(0, 0, 5);
INSERT INTO bot(id, owner_id, program_id) VALUES (5, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 6
INSERT INTO cell(x, y, entity_id) VALUES(0, 1, 6);
INSERT INTO bot(id, owner_id, program_id) VALUES (6, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 7
INSERT INTO cell(x, y, entity_id) VALUES(1, -1, 7);
INSERT INTO bot(id, owner_id, program_id) VALUES (7, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 8
INSERT INTO cell(x, y, entity_id) VALUES(1, 0, 8);
INSERT INTO bot(id, owner_id, program_id) VALUES (8, 1, 1);
INSERT INTO entity(type) VALUES ('bot'); -- 9
INSERT INTO cell(x, y, entity_id) VALUES(1, 1, 9);
INSERT INTO bot(id, owner_id, program_id) VALUES (9, 1, 1);
